package enfers;

import hf.Hf;
import hf.Mode;
import hf.mode.GameMode;
import hf.entity.Bad;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import etwin.ds.WeakMap;

@:build(patchman.Build.di())
class MiscAtlas {

	@:diExport
	public var patch(default, null): IPatch;

	public function new(): Void {
    	this.patch = new PatchList([
            
            Ref.auto(Bad.dropReward).wrap(function(hf, self: Bad, old): Void {
                
                var fl_mainWorld = self.game.world.fl_mainWorld;
                var currentId = self.game.world.currentId; 
                
                if(!fl_mainWorld && currentId == 71) {
                    return null;
                } else {
                    old(self);
                }
            }),
    	]);
  	}
}