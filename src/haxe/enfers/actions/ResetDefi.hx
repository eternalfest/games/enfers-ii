package enfers.actions;

import hf.Hf;
import hf.Mode;
import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class ResetDefi implements IAction {
  public var name(default, null): String = Obfu.raw("resetAndGoToPreviousLevel");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
    var game: GameMode = ctx.getGame();
    var id: Int = game.world.currentId;
      
    game.world.fl_read[id] = game.world.fl_visited[id] = false;
    
    //game.world.goto(game.world.currentId - 1);
    game.fl_clear = true;
    game.usePortal(10, null);
      
    return false;
  }
}