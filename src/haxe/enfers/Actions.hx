package enfers;

import etwin.ds.FrozenArray;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import merlin.IAction;
import merlin.IRefFactory;
import enfers.actions.Hitbox;
import enfers.actions.ResetDefi;

@:build(patchman.Build.di())
class Actions {
  @:diExport
  public var actions(default, null): FrozenArray<IAction>;

  @:diExport
  public var patches(default, null): FrozenArray<IPatch>;
  @:diExport
  public var hitbox(default, null): IAction;
  @:diExport
  public var resetDefi(default, null): IAction;

  public function new() {
    this.patches = FrozenArray.from(patches);
    this.hitbox = new Hitbox();
    this.resetDefi = new ResetDefi();
  }
}
